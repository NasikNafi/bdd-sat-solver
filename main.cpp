#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <chrono>
#include <map>

using namespace std;

int nVar, nClause;
vector<vector<int>> clauseList;
enum BooleanOpr {
    AND, OR
};

struct node {
    int var;
    struct node *lowChild;
    struct node *highChild;
};

int found = 0;
map<int, bool> solution;
auto t1 = chrono::high_resolution_clock::now();

int read_input(char *fileName) {
    ifstream iFile(fileName);
    char temp[10];
    string str;

    // reading comments and parameters
    streampos oldpos;
    while (getline(iFile, str) && (str[0] == 'c' || str[0] == 'p')) {
        oldpos = iFile.tellg();
        if (str[0] == 'p') {
            string buf;
            stringstream ss(str);
            vector<string> tokens;
            while (ss >> buf)
                tokens.push_back(buf);
            nVar = stoi(tokens[tokens.size() - 2]);
            nClause = stoi(tokens[tokens.size() - 1]);
        }
    }

    // reading clauses
    iFile.seekg(oldpos);
    vector<int> singleClause;
    while (iFile >> temp) {
        // reading a clause with terminating 0
        if (temp[0] == '0') {
            if (singleClause.size() > 0) {
                clauseList.push_back(singleClause);
                singleClause.clear();
            }
        } else {
            int x = atoi(temp);
            singleClause.push_back(x);
        }
    }
    iFile.close();
    return 0;
}

bool preceeds(int i, int j) {
    if (i < j)
        return true;
    else
        return false;
}

void free_tree(struct node *n) {
    struct node *l = n->lowChild;
    struct node *r = n->highChild;
    free(n);
    if (l != NULL)
        free_tree(l);
    if (r != NULL)
        free_tree(r);
}

struct node *create_terminal_zero_node() {
    struct node *n = NULL;
    n = (node *) malloc(sizeof(struct node));
    n->var = 0;
    n->lowChild = NULL;
    n->highChild = NULL;
    return n;
}

struct node *create_terminal_one_node() {
    struct node *n = NULL;
    n = (node *) malloc(sizeof(struct node));
    n->var = 1;
    n->lowChild = NULL;
    n->highChild = NULL;
    return n;
}

struct node *create_BDD_for_one_nonterminal_node(int l) {
    struct node *n = NULL;
    n = (node *) malloc(sizeof(struct node));
    // need to be modified to get clause literal
    if (l > 0) {
        n->var = l;
        n->lowChild = create_terminal_zero_node();
        n->highChild = create_terminal_one_node();
    } else {
        n->var = -l;
        n->lowChild = create_terminal_one_node();
        n->highChild = create_terminal_zero_node();
    };

    return n;
}

bool isTerminal(struct node *n) {
    if (n->lowChild == NULL && n->highChild == NULL)
        return true;
    else
        return false;
}

struct node *restrictedBdd(struct node *b, int var, int val) {
    if (val == 0) {
        return b->lowChild;
    } else {
        return b->highChild;
    }
}

struct node *clone_tree(struct node *t) {
    if (t == NULL) return NULL;

    struct node *newNode = NULL;
    newNode = (node *) malloc(sizeof(struct node));
    newNode->var = t->var;
    newNode->lowChild = clone_tree(t->lowChild);
    newNode->highChild = clone_tree(t->highChild);
    return newNode;
}

struct node *create_BDD(struct node *b1, struct node *b2, BooleanOpr opr) {

    if (isTerminal(b1) && isTerminal(b2)) {
        if (opr == AND) {
            if (b1->var && b2->var) {
                return clone_tree(b1);
            } else {
                if (b1->var == 0) {
                    return clone_tree(b1);
                } else {
                    return clone_tree(b2);
                }
            }
        } else if (opr == OR) {
            if (b1->var || b2->var)
                if (b1->var == 1) {
                    return clone_tree(b1);
                } else {
                    return clone_tree(b2);
                }
            else {
                if (b1->var == 0) {
                    return clone_tree(b1);
                } else {
                    return clone_tree(b2);
                }
            }
        }
    } else if (isTerminal(b1)) {
        if (opr == AND) {
            if (b1->var == 0) {
                return clone_tree(b1);
            } else {
                return clone_tree(b2);
            }
        } else if (opr == OR) {
            if (b1->var == 0) {
                return clone_tree(b2);
            } else {
                return clone_tree(b1);
            }
        }
    } else if (isTerminal(b2)) {
        if (opr == AND) {
            if (b2->var == 0) {
                return clone_tree(b2);
            } else {
                return clone_tree(b1);
            }
        } else if (opr == OR) {
            if (b2->var == 0) {
                return clone_tree(b1);
            } else {
                return clone_tree(b2);
            }
        }
    } else if (b1->var == b2->var) {
        struct node *n = NULL;
        n = (node *) malloc(sizeof(struct node));

        struct node *b1Res0 = clone_tree(restrictedBdd(b1, n->var, 0));
        struct node *b1Res1 = clone_tree(restrictedBdd(b1, n->var, 1));
        struct node *b2Res0 = clone_tree(restrictedBdd(b2, n->var, 0));
        struct node *b2Res1 = clone_tree(restrictedBdd(b2, n->var, 1));

        n->var = b1->var;
        n->lowChild = create_BDD(b1Res0, b2Res0, opr);
        n->highChild = create_BDD(b1Res1, b2Res1, opr);

        free_tree(b1Res0);
        free_tree(b1Res1);
        free_tree(b2Res0);
        free_tree(b2Res1);
        return n;
    } else if (preceeds(b1->var, b2->var)) {
        struct node *n = NULL;
        n = (node *) malloc(sizeof(struct node));

        struct node *b1Res0 = clone_tree(restrictedBdd(b1, n->var, 0));
        struct node *b1Res1 = clone_tree(restrictedBdd(b1, n->var, 1));

        n->var = b1->var;
        n->lowChild = create_BDD(b1Res0, b2, opr);
        n->highChild = create_BDD(b1Res1, b2, opr);

        free_tree(b1Res0);
        free_tree(b1Res1);
        return n;
    } else {
        struct node *n = NULL;
        n = (node *) malloc(sizeof(struct node));

        struct node *b2Res0 = clone_tree(restrictedBdd(b2, n->var, 0));
        struct node *b2Res1 = clone_tree(restrictedBdd(b2, n->var, 1));

        n->var = b2->var;
        n->lowChild = create_BDD(b1, b2Res0, opr);
        n->highChild = create_BDD(b1, b2Res1, opr);

        free_tree(b2Res0);
        free_tree(b2Res1);
        return n;
    }
}

void print_tree(struct node *n) {
    if (n->lowChild != NULL)
        print_tree(n->lowChild);

    if (n->lowChild != NULL)
        cout << " " << n->var << " ";
    else
        cout << " l" << n->var << "l ";

    if (n->highChild != NULL)
        print_tree(n->highChild);
}

struct node *create_BDD_for_clause(int i, int len) {
    if (len == 1)
        return create_BDD_for_one_nonterminal_node(clauseList[i][len - 1]);
    else {
        struct node *b1 = create_BDD_for_one_nonterminal_node(clauseList[i][len - 1]);
        struct node *b2 = create_BDD_for_clause(i, len - 1);
        struct node *res = create_BDD(b1, b2, OR);
        free_tree(b1);
        free_tree(b2);
        return res;
    }

}

struct node *create_BDD_for_formula() {
    struct node *b1 = NULL;
    struct node *b2 = NULL;
    struct node *result = NULL;

    b1 = create_BDD_for_clause(0, clauseList[0].size());
    for (int i = 0; i < clauseList.size() - 1; ++i) {
        if (i != 0)
            b1 = result;
        b2 = create_BDD_for_clause(i + 1, clauseList[i + 1].size());
        result = create_BDD(b1, b2, AND);
        free_tree(b1);
        free_tree(b2);
    }
    return result;
}

void check_solution(struct node* t){
    if (found == 1)
        return;
    if(t->lowChild == NULL && t->highChild == NULL){
        if(t->var == 1){
            found = 1;
            return;
        }
    }
    else{
        check_solution(t->lowChild);
        if(found == 1){
            solution.insert(pair<int, bool>(t->var, false));
            return;
        }

        check_solution(t->highChild);
        if(found == 1){
            solution.insert(pair<int, bool>(t->var, true));
            return;
        }
    }
}

void print_result(bool result) {
    auto t2 = chrono::high_resolution_clock::now();
    chrono::duration<double, milli> elapsed = t2 - t1;
    cout << "c Given formula has " << nVar << " variables and " << nClause << " clauses." << endl;
    if (result) {
        cout << "s SATISFIABLE" << endl;
        cout << "v ";
        for (pair<int, bool> element : solution) {
            if (element.second) {
                cout << element.first << " ";
            } else {
                cout << element.first * -1 << " ";
            }
        }
        cout << "0" << endl;
    } else {
        cout << "s UNSATISFIABLE" << endl;
    }
    cout << "c Done (mycputime is " << elapsed.count() / 1000 << "s)." << endl;
}

int main(int argc, char *argv[]) {
    // getting command line argument
    char fileName[100];
    if (argc == 2) {
        strncpy(fileName, argv[1], 100);
    } else {
        cout << "Provide required number of arguments correctly." << endl;
    }

    if (read_input(fileName) == -1) {
        return 0;
    }

    struct node *b = NULL;
    b = create_BDD_for_formula();

    check_solution(b);
    if(found)
        print_result(true);
    else
        print_result(false);
    free_tree(b);

    return 0;
}